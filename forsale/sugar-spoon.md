---
title: Hand-Carved Wooden Sugar Spoon - SOLD
footnote:
images: 
    - ../images/sugarspoon.jpeg
---
Hand-carved small basswooden sugar spoon. Treated with food-grade mineral oil.

£15 shipped anywhere in the world
